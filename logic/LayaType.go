package logic

func LayaString(classStr string) string {

	switch classStr {
	case "KeyboardEvent":
		classStr = "Event"
		break
	case "MouseEvent":
		classStr = "Event"
		break
	case "Event":
		classStr = "Event"
		break

	case "DisplayObject":
		classStr = "Sprite"
		break

	}

	return "Laya." + classStr + ";"

}
