package logic

import (
	"as2ts/util"
	"fmt"
	"strings"
)

type classType string

const Sprite classType = "Sprite"

type CodeStringType int8

const (
	CodeType CodeStringType = 0
	DesType  CodeStringType = 1
)

/**
代码行
*/
type AsCodeLine struct {
	codestr string
	strType CodeStringType ///0为代码

	ascriptionFun  string
	ascriptionClas string //归属那个类
}

type AsFunData struct {
	funName string
	funStr  string //内容

}

type AsFileData struct {
	packagestr string
	classname  string

	url string

	//targeturl string

	importClass []string

	ParentClass string

	content string //原文件内容

	tsContent string //转成ts后的内容

	StaticVarArr []string
	VarArr       []string
	FunArr       []string

	lineMap  []string         //拆成行
	lineWord map[int][]string //行拆成单词

	lineCodeArr []AsCodeLine //代码标记行

	NotWrite int ///是否要写文件 0写  1不写

}

func (s *AsFileData) setClassName(value string) {
	s.classname = value
}

func (s *AsFileData) FindVarFun() {
	if s.lineWord == nil {
		s.lineWord = make(map[int][]string)
	}
	s.ParentClass = getParentClass(s.content)

	s.content = AddEmpty(s.content)

	s.lineMap = getFileArr(s.content)

	classLen := len(getClassKeyArr(s.content))
	if classLen > 1 {

		fmt.Println("more than one class ")
		fmt.Println(s.url)

		return
	}

	s.FunArr = getFileFunNameArr(s.content)

	s.StaticVarArr = getFileStaticVarNameArr(s.content)

	s.VarArr = getFileVarNameArr(s.content,s.StaticVarArr)


}

func (s *AsFileData) StartConvertTots() {
	s.lineMap = getFileArr(s.content)
	filerow := len(s.lineMap)

	resultStr := ""
	funcCount := len(s.FunArr)
	varCount := len(s.VarArr)
	staticCount := len(s.StaticVarArr)
	//
	result := 0

	for index := 0; index < filerow; index++ {
		tmpstr := s.lineMap[index]

		if getNeedNotes(tmpstr) > 0 {
			resultStr += "\n"
			resultStr += "//" + tmpstr
			resultStr += "\n"
			continue
		}

		for i := 0; i < funcCount; i++ {
			funcName := s.FunArr[i]
			if funcName == s.classname {
				continue
			}

			tmpstr, result = handleFunStr(tmpstr, funcName)
			if result == 1 {
				break
			}

		}



		//
		////变量
		result = checkDeclareWord(tmpstr)




		if result == 0 {

			tmpArr := GetLineWordArr(tmpstr)

			if len(tmpArr) > 0 {

				tmpstr = ""

				for j := 0; j < len(tmpArr); j++ {
					tmpvarstr := tmpArr[j]

					if getKeyWord(tmpvarstr) > 1 {
						continue
					}

					iscurrent := 0
					for i := 0; i < varCount; i++ {

						if tmpvarstr == s.VarArr[i] {
							tmpindex:=j-2
							if len(tmpArr)>tmpindex&&tmpindex>=0&& tmpArr[tmpindex] == "this"{
								//println("")
							}else {
								tmpArr[j] = "this." + s.VarArr[i]
								iscurrent = 1
							}

						}
					}

					for k := 0; k < staticCount; k++ {
					 	tmpStaticVar:=s.StaticVarArr[k]
						if tmpvarstr ==tmpStaticVar {
							tmpArr[j] = s.classname+"." + tmpStaticVar
							iscurrent = 1
						}
					}

					if iscurrent == 0 {

						tmpArr[j] = getParentClassVar(tmpvarstr, s.ParentClass)
					}

					if tmpArr[j] != "" {
						tmpIndex := j + 1



						if tmpIndex < len(tmpArr) && tmpArr[j] == "-"  && tmpArr[tmpIndex] == "=" {
							//-=
							tmpstr += tmpArr[j]
						}else  if tmpIndex < len(tmpArr) && tmpArr[j] == "+" && tmpArr[tmpIndex] == "=" {
							//+=
							tmpstr += tmpArr[j]
						}else if tmpIndex < len(tmpArr) && tmpArr[j] == "!"   && tmpArr[tmpIndex] == "=" {
							//!=
							tmpstr += tmpArr[j]
						}else if tmpIndex < len(tmpArr) && tmpArr[j] == "<"  && tmpArr[tmpIndex] == "=" {
							//<=
							tmpstr += tmpArr[j]
						}else if tmpIndex < len(tmpArr) && tmpArr[j] == ">"   && tmpArr[tmpIndex] == "=" {
						//>=
							tmpstr += tmpArr[j]
						}else if tmpIndex < len(tmpArr) && tmpArr[j] == "=" &&   tmpArr[tmpIndex] == "=" {
							//两等号问题 在数组里是 = "" =
							tmpstr += tmpArr[j]
						} else {
							tmpstr += tmpArr[j] + " "
						}

					}

				}

				//this.data = this.data
				if strings.Index(tmpstr, "=") > -1 {
					tmpstr = handleSetVar(tmpstr)
				}

			}

		}

		//
		//
		//
		//for j := 0; j < staticCount; j++ {
		//	tmpstr = handleStaticVarStr(tmpstr, s.StaticVarArr[j], s.classname)
		//}

		tmpfilestr, isExist := AppDataMgrInstance().GetAppFileStr(s.classname)
		if isExist {
			tmpstr = handlePackageToNamespace(tmpstr, tmpfilestr.NameSpace)
		}

		//
		tmpstr = handleImportStr(tmpstr)

		tmpstr = handleAuthorInfo(tmpstr)

		tmpstr = convertTots(tmpstr)

		if index > 0 {
			resultStr += "\n"
		}

		//屏蔽字段
		tmpstr = shieldRow(tmpstr)

		resultStr += tmpstr

	}

	s.content = handleClassName(resultStr, s.classname)
}

func getParentClassVar(varstr string, parentClass string) string {
	tmpfile, isExist := AppDataMgrInstance().GetAppFileStr(parentClass)
	if !isExist {
		return varstr
	}

	varCount := len(tmpfile.File.VarArr)

	VarArr := tmpfile.File.VarArr
	for i := 0; i < varCount; i++ {

		if varstr == VarArr[i] {
			return "this." + VarArr[i]

		}
	}

	if tmpfile.File.ParentClass != "" {
		varstr = getParentClassVar(varstr, tmpfile.File.ParentClass)
	}

	return varstr
}

func (s *AsFileData) SetFileUrl(value string) {
	s.url = value
}

func (s *AsFileData) SetClassName(value string) {
	s.classname = value
}

func (s *AsFileData) SetData(content string) {

	s.content = content

}

/**
是否是注释
用于单行
*/
func (s *AsFileData) isDes(str string) int {
	str = compressStr(str)

	if str == "" {
		return 0
	}

	tmplen := len(str)
	if tmplen < 2 {
		return 0
	}

	tmp := str[0]
	tmp1 := str[1]

	//   //
	if tmp == 47 && tmp1 == 47 {
		return 1
	}

	//  /  47
	// *  42

	lastTmp := str[tmplen-1]
	lastTmp1 := str[tmplen-2]

	if tmp == 47 && tmp1 == 42 && lastTmp1 == 42 && lastTmp == 47 {
		//fmt.Println("单行注释start  /*****/")
		//fmt.Println(str)
		return 1
	}

	return 0
}

func (s *AsFileData) isMultilineDesStart(str string) int {
	if str == "" {
		return 0
	}

	if len(str) < 2 {
		return 0
	}

	tmp := str[0]  //  /  47
	tmp1 := str[1] // *  42

	if tmp == 42 && tmp1 == 47 {
		//fmt.Println("多行注释start")
		//fmt.Println(str)
		return 1
	}

	return 0
}

func (s *AsFileData) isMultilineDesEnd(str string) int {
	if str == "" {
		return 0
	}

	if len(str) < 2 {
		return 0
	}

	tmp1 := str[0] // *  42
	tmp := str[1]  //  /  47

	if tmp == 42 && tmp1 == 47 {
		//fmt.Println("多行注释end")
		//fmt.Println(str)
		return 1
	}

	return 0
}

func (s *AsFileData) setLineCode() {
	maplen := len(s.lineMap)

	s.lineCodeArr = make([]AsCodeLine, maplen) //

	clasName := s.classname

	///单行
	for index := 0; index < maplen; index++ {

		tmpstr := s.lineMap[index]
		linecode := AsCodeLine{}

		if s.isDes(tmpstr) > 0 {
			linecode.strType = DesType
		} else {
			clasArr := getClassKeyArr(tmpstr)
			if len(clasArr) > 0 {
				clasName = clasArr[0]
			}

			tmpstr = AddEmpty(tmpstr)
		}

		linecode.codestr = tmpstr

		linecode.ascriptionClas = clasName

		s.lineCodeArr[index] = linecode

	}

	//多行
	isDes := 0

	for index := 0; index < maplen; index++ {

		linecode := s.lineCodeArr[index]
		if linecode.strType == DesType {
			continue
		}

		tmpstr := linecode.codestr

		if s.isMultilineDesStart(tmpstr) > 1 {
			isDes = 1
		}

		if isDes == 0 {
			linecode.strType = CodeType
		} else {
			linecode.strType = DesType
		}

		if s.isMultilineDesEnd(tmpstr) > 1 {
			isDes = 0
		}
	}

}

/**检查类
看是不是有两个类在一个文件里
如果有，就帮手独立出来
*/
func (s *AsFileData) CheckOutClass() {

	if s.lineWord == nil {
		s.lineWord = make(map[int][]string)
	}

	//
	startindex := strings.LastIndex(s.url, util.GetPath()) + 1

	eindex := strings.LastIndex(s.url, ".")
	s.classname = s.url[startindex:eindex]

	///get package
	startindex = strings.LastIndex(s.url, "src") + 4
	eindex = strings.LastIndex(s.url, s.classname) - 1

	packageUrl := s.url[startindex-4 : eindex+1]

	if startindex < eindex {
		s.packagestr = s.url[startindex:eindex]
		s.packagestr = strings.Replace(s.packagestr, "/", ".", -1)
	}

	s.lineMap = getFileArr(s.content)
	s.setLineCode()

	clasNameArr := getClassKeyArr(s.content)
	classLen := len(clasNameArr)
	if classLen == 0 {
		return
	}

	fileStr, isExist := AppDataMgrInstance().GetAppFileStr(s.classname)
	if !isExist {
		println("分离文件时未找到文件")
		return
	}

	tmpNameSpace := fileStr.NameSpace
	tmpTargetDir := fileStr.Targetdirurl

	AppDataMgrInstance().DeleteAppFileStr(s.classname)

	startindex = strings.LastIndex(s.url, "/") + 1
	newclassDirUrl := s.url[0:startindex]

	for i := 0; i < classLen; i++ {
		className := clasNameArr[i]

		classStr := ""
		filelen := len(s.lineCodeArr)
		for index := 0; index < filelen; index++ {
			linecode := s.lineCodeArr[index]
			if linecode.ascriptionClas == className {

				classStr += linecode.codestr + "\n"
			}
		}

		if getPackageKey(classStr) == "" {
			//没有包头
			classStr = "package " + s.packagestr + "\n{\n" + classStr + "\n}"
		}

		classUrl := newclassDirUrl + className + ".as"

		tmpTargetUrl := tmpTargetDir + packageUrl + className + ".ts"

		asFileData := new(AsFileData)
		asFileData.SetData(classStr)
		asFileData.classname = className
		asFileData.SetFileUrl(classUrl)

		AppDataMgrInstance().AddClassName(className,
			tmpNameSpace, tmpTargetDir,
			tmpTargetUrl, classUrl, asFileData)
	}

}

func (s *AsFileData) GetFileData() string {
	return s.content
}

func (s *AsFileData) SetFileData(value string) {
	s.content = value
}

//func (s *AsFileData) Gettargeturl() string {
//	return s.targeturl
//}
