package main

import (
	"as2ts/logic"

	"as2ts/util"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"strings"

	"regexp"
	"sync"
	"time"

	"pangu/cfile"
)

type Config struct {
	SourcefileUrl string
	SourceType    string

	TargetUrl string
	OutType   string

	ShieldPackage string

	ChangeTo string
}

var wg sync.WaitGroup

var writefileurl string

var config Config
var shieldPackageArr []string

var sourcefileUrlArr []string

func main() {



	config = loadConfig()



	//runtime.GOMAXPROCS(runtime.NumCPU()*2)

	fmt.Println("---start main---")

	fmt.Printf("read file\n")

	startTime := time.Now()


	starthandle()
	

	constTime := time.Since(startTime)
	fmt.Println("cost=[%s]", constTime)

}

func starthandle()  {

	logic.AppDataMgrInstance().ChangeTo = config.ChangeTo

	writefileurl = config.TargetUrl
	cfile.RemoveDir(writefileurl)
	cfile.Mkdir(writefileurl)

	for i := 0; i < len(sourcefileUrlArr); i++ {

		list, err := cfile.DirList(sourcefileUrlArr[i])
		if err != nil {
			fmt.Println(err)
			return
		}

		initApp(list)
	}

}

func loadConfig() Config {

	content, err := ioutil.ReadFile("config.xml")
	if err != nil {
		panic(err)
	}

	// xml 解析到result的结构中
	var result Config
	err = xml.Unmarshal(content, &result)

	if err != nil {
		panic(err)
	}

	tmpstr:=result.SourcefileUrl
	sourcefileUrlArr=strings.Split(tmpstr,",")

	shieldPackageArr = strings.Split(result.ShieldPackage, ",")

	return result
}

func initApp(list []string) {

	sourceType := `\.` + config.SourceType
	//reg := regexp.MustCompile(`src\/[\w+/*]+`)
	filesortReg := regexp.MustCompile(sourceType)

	for _, v := range list {

		if len(filesortReg.FindAllString(v, -1)) == 0 {
			fmt.Println("不用转换的文件", v)
			continue
		}

		if isShieldPackage(v) {
			fmt.Println("不用转换库的文件", v)
			continue
		}

		
		tmpIndex:= strings.LastIndex(v, util.GetPath())
		lastIndex:=strings.LastIndex(v,".")

		tmpfilename:= v[tmpIndex+1:lastIndex]

		targetdirurl := writefileurl + util.GetPath()
		targeturl := targetdirurl + tmpfilename + "." + config.OutType

		tmpfilename = strings.Replace(tmpfilename, "src/", "", -1)

		tindex := strings.Index(tmpfilename, "/")
		tmpNameSpace:=""
		if(tindex>0){
			tmpNameSpace = tmpfilename[0:tindex]
		}


		tmpindex := strings.LastIndex(tmpfilename, "/") + 1
		tmplen := len(tmpfilename)
		tmpclassName := tmpfilename[tmpindex:tmplen]

		asFile := loadFile(v, tmpclassName)

		logic.AppDataMgrInstance().AddClassName(tmpclassName,
			tmpNameSpace, targetdirurl, targeturl, v, asFile)

	}

	handle := new(logic.HandleAsLogic)
	handle.StartHandle()
}

func loadFile(fileUrl string, className string)* logic.AsFileData {
	wfile := new(cfile.File)

	filestr := wfile.ReadFile(fileUrl)

	///asFileData := logic.AsFileData{}///创建一个实例
	asFileData :=new( logic.AsFileData )//创建一个带指针的实例

	asFileData.SetData(filestr)
	asFileData.SetClassName(className)
	asFileData.SetFileUrl(fileUrl)
	return asFileData
}

func isShieldPackage(str string) bool {
	tmplen := len(shieldPackageArr)
	for i := 0; i < tmplen; i++ {

		if strings.Index(str, shieldPackageArr[i]) > -1 {
			return true
		}
	}

	return false
}
