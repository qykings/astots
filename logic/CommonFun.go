package logic

import (
	"regexp"
	"strings"
)

/**关键字--有些不精确，有空提高精准度，提升性能
**/
func getKeyWord(str string) int {
	str = deleteSpace(str)

	keystr := "as,break,case,catch,class,const,continue,default,delete,do,else,extends,false,finally,for,function,if,implements,import,in,instanceof,interface,internal,is,native,new,null,package,private,protected,public,return,super,switch,this,throw,to,true,try,typeof,use,var,void,while,with"

	tmpArr := strings.Split(keystr, ",")
	for i := 0; i < len(tmpArr); i++ {
		if tmpArr[i] == str {
			return 1
		}
	}

	return 0
}

func getParentClass(str string) string {
	index := strings.Index(str, "extends")

	if index == -1 {
		return ""
	}
	tmplen := len("extends")

	tmpstr := str[index:len(str)]
	implements := strings.Index(tmpstr, "implements")
	eindex := 0

	if implements > -1 {
		eindex = implements
	} else {
		eindex = strings.Index(tmpstr, "{")
	}

	tmpstr = tmpstr[tmplen:eindex]
	str = deleteSpace(tmpstr)

	return str
}

/**
分成行
*/
func getFileArr(str string) []string {
	strArr := strings.Split(str, "\n")
	return strArr
}

func getPackageKey(str string) string {

	reg := regexp.MustCompile(`(package)\s*[\w]+`)

	funResult := reg.FindAllString(str, -1)
	funLen := len(funResult)
	if funLen == 0 {
		return ""
	}

	return str
}

/**含有class 关键字**/
func getClassKeyArr(str string) []string {
	reg := regexp.MustCompile(`(final|private|public|protected)\s*class\s*[\w]+`)

	funResult := reg.FindAllString(str, -1)
	funLen := len(funResult)
	if funLen == 0 {
		return nil
	}

	var clasArr []string

	for index := 0; index < funLen; index++ {
		tmpstr := funResult[index]

		left := strings.LastIndex(tmpstr, "class")

		tmpstr = tmpstr[left+5 : len(tmpstr)]

		tmpstr = strings.Replace(tmpstr, "\n", "", -1)
		tmpstr = deleteSpace(tmpstr)

		clasArr = append(clasArr, tmpstr)
	}

	return clasArr
}

func deleteSpace(str string) string {
	str = strings.Replace(str, " ", "", -1)
	// 去除换行符
	str = strings.Replace(str, "\n", "", -1)
	str = strings.Replace(str, "\t", "", -1)
	return str
}

//
//func getClassNameArr(str string) []string {
//	reg := regexp.MustCompile(`(final|private|public|protected)\s*class\s*[\w]+`)
//
//	funResult := reg.FindAllString(str, -1)
//	funLen := len(funResult)
//	if funLen == 0 {
//		return nil
//	}
//
//	var clasArr []string
//
//	for index := 0; index < funLen; index++ {
//		tmpstr := funResult[index]
//
//		tmpstr = strings.Replace(tmpstr, "class", "", -1)
//		tmpstr = strings.Replace(tmpstr, "\n", "", -1)
//		tmpstr = strings.Replace(tmpstr, " ", "", -1)
//
//		clasArr = append(clasArr, tmpstr)
//	}
//
//	return clasArr
//}

func getFileFunNameArr(str string) []string {
	var funArr []string

	//识别方法 在调用时加this
	reg := regexp.MustCompile(`(private|public|protected)\s*function\s*[\w]+\(`)
	funResult := reg.FindAllString(str, -1)
	funLen := len(funResult)
	if funLen > 0 {

		for index := 0; index < funLen; index++ {
			cellstr := funResult[index]
			ireg := regexp.MustCompile(`\w+`)
			cellimportArr := ireg.FindAllString(cellstr, -1)

			lastindex := len(cellimportArr) - 1
			funcName := cellimportArr[lastindex]

			funArr = append(funArr, funcName)
		}
	}
	return funArr
}

func getFileStaticVarNameArr(str string) []string {

	//识别当前类全局变量
	reg := regexp.MustCompile(`static\s*(private|public|protected)\s*const\s*[\w]+\s*\:`)
	funResult := reg.FindAllString(str, -1)
	rlen := len(funResult)
	var rstrArr []string

	if rlen > 0 {
		var cellstr = ""
		for index := 0; index < rlen; index++ {
			cellstr = funResult[index]
			ireg := regexp.MustCompile(`\w+`)
			varnameArr := ireg.FindAllString(cellstr, -1)

			lastindex := len(varnameArr) - 1
			varName := varnameArr[lastindex]
			rstrArr = append(rstrArr, varName)
		}
	}

	reg = regexp.MustCompile(`static\s*(private|public|protected)\s*var\s*[\w]+\s*:`)
	funResult = reg.FindAllString(str, -1)
	rlen = len(funResult)
	if rlen > 0 {
		var cellstr = ""
		for index := 0; index < rlen; index++ {
			cellstr = funResult[index]
			ireg := regexp.MustCompile(`\w+`)
			varnameArr := ireg.FindAllString(cellstr, -1)

			lastindex := len(varnameArr) - 1
			varName := varnameArr[lastindex]
			rstrArr = append(rstrArr, varName)
		}
	}

	reg = regexp.MustCompile(`(private|public|protected)\s*static\s*var\s*[\w]+\s*:`)
	funResult = reg.FindAllString(str, -1)
	rlen = len(funResult)
	if rlen > 0 {
		var cellstr = ""
		for index := 0; index < rlen; index++ {
			cellstr = funResult[index]
			ireg := regexp.MustCompile(`\w+`)
			varnameArr := ireg.FindAllString(cellstr, -1)

			lastindex := len(varnameArr) - 1
			varName := varnameArr[lastindex]
			rstrArr = append(rstrArr, varName)
		}
	}

	return rstrArr
}

/**
localVar 本类的变量
*/
func getFileVarNameArr(str string, localVar []string) []string {

	//识别当前类全局变量
	reg := regexp.MustCompile(`(private|public|protected)\s*var\s*[\w]+\:`)
	funResult := reg.FindAllString(str, -1)
	rlen := len(funResult)
	var rstrArr []string

	if rlen > 0 {

		for index := 0; index < rlen; index++ {
			cellstr := funResult[index]
			ireg := regexp.MustCompile(`\w+`)
			varnameArr := ireg.FindAllString(cellstr, -1)

			lastindex := len(varnameArr) - 1
			varName := varnameArr[lastindex]

			isExist := 0
			for i := 0; i < len(localVar); i++ {
				if localVar[i] == varName {
					isExist = 1
				}
			}
			if(isExist==0){
				rstrArr = append(rstrArr, varName)
			}

		}
	}
	return rstrArr
}

func handlePackageToNamespace(str string, namespace string) string {
	reg := regexp.MustCompile(`package+\s`)
	result := reg.FindAllString(str, -1)

	if result == nil {
		return str
	}

	resultstr := ""
	if len(result) > 0 {
		resultstr = "namespace " + namespace + " "
	}

	//处理没有换行的{
	///package xxxx.ActorClass {
	if strings.Index(str, "{") > -1 {
		resultstr += "\n{"

	}

	return resultstr
}

func handleImportStr(str string) string {

	reg := regexp.MustCompile(`import+\s[\w]+.*`)
	importResult := reg.FindAllString(str, -1)
	importworklen := len(importResult)
	if importworklen > 0 {
		importstr := ""
		for index := 0; index < importworklen; index++ {

			cellstr := importResult[index]

			ireg := regexp.MustCompile(`\w+`)
			cellimportArr := ireg.FindAllString(cellstr, -1)

			lastindex := len(cellimportArr) - 1
			lastWord := cellimportArr[lastindex]

			if cellimportArr[1] == "flash" {

				if AppDataMgrInstance().ChangeTo == "laya" {
					importstr += "	import " + lastWord + "=" + LayaString(lastWord)
				} else {
					importstr += "	import " + lastWord + "=" + EgretString(lastWord)
				}

			} else if cellimportArr[1] == "laya" {
				importstr += "	import " + lastWord + "=" + LayaString(lastWord)
			} else if cellimportArr[1] == "fairygui" {
				importstr += "	import " + lastWord + "=" + "fgui." + lastWord
			} else {

				tmpfilestr, isExist := AppDataMgrInstance().GetAppFileStr(lastWord)

				if !isExist {
					println("handleImportStr---no find class", lastWord)
				}

				cellstr = regexpstr("import", cellstr, "")

				if tmpfilestr.NameSpace == "" {

					importstr += "	import " + lastWord + "=" + lastWord
				} else {
					importstr += "	import " + lastWord + "=" +
						tmpfilestr.NameSpace + "." + lastWord
				}

			}

		}
		str = importstr

	}
	return str
}

func handleStaticVarStr(str string, varname string, classname string) string {

	//reg := regexp.MustCompile(`(package|private|public|protected)\s`)
	//funResult := reg.FindAllString(str, -1)
	//rlen := len(funResult)
	//if rlen > 0 {
	//	return str
	//}
	//
	//reg = regexp.MustCompile(`\s*` + varname + `\s*`)
	//funResult = reg.FindAllString(str, -1)
	//
	//if len(funResult) > 0 {
	//
	//	str = regexpstr(`\s`+varname, str, " "+classname+"."+varname)
	//
	//	str = regexpstr(`\(`+varname, str, "( "+classname+"."+varname+" ")
	//
	//	str = regexpstr(`\=`+varname, str, "= "+classname+"."+varname+" ")
	//
	//	str = regexpstr(`\|`+varname, str, "| "+classname+"."+varname+" ")
	//
	//	// (!sceneid)
	//	str = regexpstr(`\(\!`+classname+"."+varname, str, "( !"+classname+"."+varname+" ")
	//
	//}

	return str
}

/**
拆成单字数组
*/
func GetLineWordArr(str string) []string {
	words := strings.Fields(str)

	tmpLen := len(words)
	if tmpLen < 1 {
		return words
	}

	keyArr := []string{".", ";", "(", ")", "<", ">", "=", "!", "{", "}", ":", ",", "[", "]"}
	keylen := len(keyArr)
	tmplen := len(words)
	i := 0
	for {
		if i >= tmplen {
			break
		}

		tmpstr := words[i]
		for j := 0; j < keylen; j++ {
			keystr := keyArr[j]
			if keystr == tmpstr {
				continue
			}
			tmpindex := strings.Index(tmpstr, keystr)
			if tmpindex > -1 {

				tleftArr := words[0:i]

				var trightArr = make([]string, len(words))
				copy(trightArr, words[i+1:])
				tmpArr := strings.Split(tmpstr, keystr)
				splitLen := len(tmpArr)

				var resultArr []string
				for i := 0; i < splitLen; i++ {

					if i == (splitLen - 1) {
						//最后一个元素不加分割符
						resultArr = append(resultArr, tmpArr[i])
					} else {
						resultArr = append(resultArr, tmpArr[i], keystr)
					}

				}

				//if tmpArr[splitLen]!=""{
				//	resultArr=append(resultArr,tmpArr[splitLen])
				//}

				words = append(tleftArr, resultArr...)

				for i := 0; i < len(trightArr); i++ {
					if trightArr[i] != "" {
						words = append(words, trightArr[i])
					}
				}

				tmplen = len(words)
				break
			}

		}

		i++

	}

	var resultWordsArr []string
	for i := 0; i < len(words); i++ {
		if words[i] == "" {
			continue
		}
		resultWordsArr = append(resultWordsArr, words[i])
	}

	return resultWordsArr
}

//处理变量 this.data=this.data
func handleSetVar(str string) string {
	tstr := strings.Replace(str, " ", "", -1)
	tmparr := strings.Split(tstr, "=")
	tmpstr := tmparr[1]

	tmpstr = strings.Replace(tmpstr, ";", "", -1)
	tmparr[1] = tmpstr

	if tmparr[0] == tmparr[1] {

		tmpstr = strings.Replace(tmpstr, "this.", "", -1)
		tmparr[1] = tmpstr
		str = ""

		str += tmparr[0] + " = " + tmparr[1]

	}

	return str
}

func checkDeclareWord(str string) int {
	reg := regexp.MustCompile(`(package|private|public|protected)\s`)
	funResult := reg.FindAllString(str, -1)
	rlen := len(funResult)
	if rlen > 0 {
		return 1
	}
	return 0
}

func handleVarStr(str string) (string, int) {

	reg := regexp.MustCompile(`(package|private|public|protected)\s`)
	funResult := reg.FindAllString(str, -1)
	rlen := len(funResult)
	if rlen > 0 {
		return str, 1
	}

	return str, 0
}

func handleVectorStr(str string, className string) string {

	reg := regexp.MustCompile(`(Vector)`)
	funResult := reg.FindAllString(str, -1)
	rlen := len(funResult)
	if rlen < 1 {
		return str
	}

	// //  ':Vector.<number> =' to 'number[] ='
	str = regexpstr(`:\s*Vector\.<(.+)>\s*=`, str, ":$1[] =")

	// //  ':Vector.<number>;' to 'number[];'
	str = regexpstr(`:\s*Vector\.<(.+)>\s*;`, str, ":$1[] ;")

	// // ': Vector.<string> {' to 'string[] {'
	str = regexpstr(`:\s*Vector\.<(.+)>\s*{`, str, ":$1[] {")

	// //  'new Vector.<uint>(7,true)' to '[]'
	str = regexpstr(`new\s+Vector\.<.+>(\(.+)\)?\;`, str, "[];")
	str = regexpstr(`new\s+Vector\.\<(.+)\>\;`, str, "[];")

	return str
}

/**
as 里有的东西，laya没有的
如URLRequest，直接给注释掉
*/
func getNeedNotes(str string) int {
	asStr := "MonsterDebugger,URLRequest,URLRequestMethod,sendToURL,Capabilities,Security"
	asArr := strings.Split(asStr, ",")

	for i := 0; i < len(asArr); i++ {
		if strings.Index(str, asArr[i]) > -1 {
			return 1
		}
	}

	return 0
}

/**
方法调用加上this
*/
func handleFunStr(str string, funcname string) (string, int) {

	///类声明免检
	reg := regexp.MustCompile(`(class)\s`)
	funResult := reg.FindAllString(str, -1)
	rlen := len(funResult)
	if rlen > 0 {
		return str, 1
	}

	//方法声明免检
	reg = regexp.MustCompile(`function\s` + funcname)
	funResult = reg.FindAllString(str, -1)
	if len(funResult) > 0 {
		return str, 1
	}

	//
	/////
	reg = regexp.MustCompile(`\.\b` + funcname + `\b`)
	funResult = reg.FindAllString(str, -1)

	if len(funResult) > 0 {
		return str, 1
	}

	//
	reg = regexp.MustCompile(`new\s*` + funcname + `\s*`)
	funResult = reg.FindAllString(str, -1)

	if len(funResult) > 0 {
		return str, 1
	}
	//

	reg = regexp.MustCompile(`\b` + funcname + `\b`)
	str = reg.ReplaceAllString(str, "this."+funcname+" ")

	return str, 0
}

func handleClassName(str string, classname string) string {
	// // constructor

	reg := regexp.MustCompile(`public\s` + classname)
	str = reg.ReplaceAllString(str, "constructor")

	return str
}

func regexpstr(regstr string, str string, replacestr string) string {
	reg := regexp.MustCompile(regstr)
	str = reg.ReplaceAllString(str, replacestr)

	return str
}

func RemoveNote(str string) string {

	startIndex := strings.Index(str, "/*")

	endIndex := strings.Index(str, "*/") + 2

	//str = str[startIndex:endIndex]
	strlen := len(str)

	leftstr := str[0:startIndex]
	rightstr := str[endIndex:strlen]

	str = leftstr + rightstr

	return str
}

func shieldRow(str string) string {

	var tmpArr = [...]string{`\s*DebuggerWin`, `\sMonsterDebugger\.`}

	tmpLen := len(tmpArr)
	for i := 0; i < tmpLen; i++ {
		if getShiel(tmpArr[i], str) >= 1 {
			return "// " + str
		}
	}

	return str

}

func getShiel(str string, findstr string) int {
	reg := regexp.MustCompile(str)
	funResult := reg.FindAllString(findstr, -1)

	return len(funResult)

}

func handleAuthorInfo(str string) string {

	reg := regexp.MustCompile(`\*\s@author\s*(\w+)`)
	funResult := reg.FindAllString(str, -1)

	if len(funResult) < 1 {
		return str
	}

	index := strings.LastIndex(str, "@author") + 7

	str = str[:index]
	str += " mykm"
	return str
}

func deleteClassName(funArr []string, clasArr []string) []string {
	for i := len(funArr) - 1; i > -1; i-- {
		tmpfunstr := funArr[i]
		for j := len(clasArr) - 1; j > -1; j-- {
			tmpclass := clasArr[j]
			//fmt.Println( tmpfunstr,"=000=",tmpclass)
			if tmpfunstr == tmpclass {
				//fmt.Println( tmpfunstr,"=1111=",tmpclass)
				funArr = append(funArr[:i], funArr[i+1:]...)
			}

			//if strings.Compare( tmpfunstr,tmpclass)==0{
			//	fmt.Println( tmpfunstr,"=2222=",tmpclass)
			//}

		}
	}

	return funArr
}

func getClassName(targeturl string) string {
	startindex := strings.LastIndex(targeturl, "/") + 1

	eindex := strings.LastIndex(targeturl, ".")
	classname := targeturl[startindex:eindex]
	return classname
}

/**
添加空格，让代码好看
*/
func AddEmpty(str string) string {

	str = brackets(str, 40)
	str = bracketsRight(str, 41)

	//str = judgestr(str, 60)
	//str = judgestr(str, 62)

	//str = regexpstr(`(\w+)+\=+(\w+)`, str, "$1 = $2")
	//str = regexpstr(`\=+(\w+)`, str, "= $1")
	//
	//str = regexpstr(`(\w+)+\=\=+(\w+)`, str, "$1 == $2")
	//str = regexpstr(`(\w+)+\!\=+(\w+)`, str, "$1 != $2")
	//
	//str = regexpstr(`\[+(\w+)`, str, "[ $1")
	//str = regexpstr(`(\w+)+\]`, str, "$1 ]")
	return str
}

//<小于 60  =等号 61   > 大于62
func judgestr(str string, value uint8) string {
	strlen := len(str)

	isadd := 0
	for i := 0; i < strlen; i++ {
		//
		if str[i] == value {
			if i+1 < strlen {
				if str[i+1] == value {
					str = str[:i+1] + " " + str[i+1:strlen]
					i = i + 1
					isadd = 1
				} else if str[i+1] == 61 {
					str = str[:i+1] + " " + str[i+1:strlen]
					i = i + 1
					isadd = 1
				}
			}

			if isadd == 1 {
				continue
			} else {
				str = str[:i] + " " + str[i+1:strlen]
			}

		}
	}

	return str
}

//)闭括号 41
func bracketsRight(str string, value uint8) string {
	strlen := len(str)

	isadd := 0
	i := 0
	for {
		strlen = len(str)

		if i >= strlen {
			break
		}
		if str[i] == value {
			if i+1 < strlen {
				//((
				if str[i+1] == value {
					str = str[:i] + " " + str[i:strlen]
					i = i + 2
					isadd = 1
				}
			}

			if isadd == 1 {
				i++
				continue
			} else {
				str = str[:i] + " " + str[i:strlen]
				i++
			}

		}
		i++

	}

	return str
}

//括号 (开括号 40
func brackets(str string, value uint8) string {
	strlen := len(str)

	i := 0
	for {
		strlen = len(str)
		if i >= strlen {
			break
		}
		if i+1 >= strlen {
			break
		}

		if str[i] == value {

			//((
			if str[i+1] == value {
				str = str[:i+2] + " " + str[i+2:strlen]
				i = i + 2

			} else {
				str = str[:i+1] + " " + str[i+1:strlen]
				i = i + 1
			}
		}
		i++

	}

	return str
}

func justLineEnd(str string) bool {
	reg := regexp.MustCompile(`(\/\*|\/\/)`)
	funResult := reg.FindAllString(str, -1)
	rlen := len(funResult)
	if rlen > 0 {
		return true
	}
	return false
}

func convertTots(str string) string {

	//clear set function  void
	str = regexpstr(`(private|public|protected)\sfunction\s+(set)+\s(.+)+(\:void)`, str, "$1 $2 $3")

	str = regexpstr(`\bBoolean\b`, str, "boolean")

	str = regexpstr(`\b(int|uint|Number)\b`, str, "number")

	// //  String to string

	str = regexpstr(`\bString\b`, str, "string")

	// //

	str = regexpstr(`:\s*Object\s*`, str, ":any")

	str = regexpstr(`new Object\(\);`, str, "{};")

	// //  * to any
	// str = str.replace(/:\s*\*/g, ":any");

	str = regexpstr(`:\s*\*\s*\;`, str, ":any;")

	str = regexpstr(`:\s*\*\s`, str, ":any ")
	//: *,
	str = regexpstr(`:\s*\*,`, str, ":any ,")

	// //  'public class' to 'export class'
	// //  'public final class' to 'export class'
	// str = str.replace(/public\s+(final\s+)?class/, "export class");
	str = regexpstr(`public\s+(final\s+)?class`, str, "export class")

	// //  'public interface' to 'export interface'
	// str = str.replace(/public\s+interface/g, "export interface");

	str = regexpstr(`public\s+interface`, str, "export interface")

	// //  'internal' to 'public'
	// str = str.replace(/\binternal\b/g, "public");

	// //  swap static order
	// str = str.replace(/static\s+(public|private|protected)/g, "$1 static");
	str = regexpstr(`static\s+(public|private|protected)`, str, "$1 static")

	// //   'public var'                   to 'public'
	// //   'public const'                 to 'public'
	// //   '(override) public function'   to 'public'
	// str = str.replace(/(override\s+)?(private|public|protected)\s+(var|const|function)/g, "$2");

	str = regexpstr(`(override\s+)?(private|public|protected)\s+(var|const|function)`, str, "$2")

	// 'public static var'       to  'public static'
	// 'public static const'     to  'public static'
	// 'public static function'  to  'public static'
	// str = str.replace(/(public|private|protected)\s+static\s+(var|const|function)/g, "$1 static");

	str = regexpstr(`(public|private|protected)\s+static\s+(var|const|function)`, str, "$1 static")

	// // local const to var
	// str = str.replace(/\bconst\b/g, "var");

	// //  'trace' to 'console.log'
	// str = str.replace(/trace\s*\(/g, "console.log(");

	str = regexpstr(`\btrace\b`, str, "console.log")

	// //  'A as B' to '<B> A'
	// str = str.replace(/(\w+)\s+(\bas\b)\s+(\w+)/g, "<$3> $1");
	// //  ':Array' to 'any[]'
	// str = str.replace(/:\s*Array/g, ":any[]");
	str = regexpstr(`:\s*Array\s`, str, ":any[]")

	str = regexpstr(`:\s*Array\s*;`, str, ":any[]")

	str = regexpstr(`:\s*Array\s*=`, str, ":any=")

	str = regexpstr(`:\s*Array\)`, str, ":any)")

	str = regexpstr(`:Array,`, str, ":any,")

	str = regexpstr(`new\sArray\(\)\;`, str, "[];")

	str = regexpstr(`\bas+\s+Array\b`, str, "")

	// //  'new <uint>[1,2,3]'   to  '[1,2,3]'
	// str = str.replace(/new\s+<.+>(\[.*\])/g, "$1");
	// //  'Vector.<uint>([1, 2, 3])' to '[1, 2, 3]'
	// str = str.replace(/(=|\s)Vector\.<.+>\((\[.*\])\)/g, "$2");

	str = regexpstr(`\s*override\s*`, str, " ")

	str = regexpstr(`\sis\s`, str, " instanceof ")

	str = regexpstr(`\sfor\seach`, str, " for ")

	str = regexpstr(`\sin\s`, str, " of ")

	//str = regexpstr(`__JS__\(\"+(.+)+\"+\)`, str, "$1")

	return str
}

//利用正则表达式压缩字符串，去除空格或制表符
func compressStr(str string) string {
	if str == "" {
		return ""
	}
	//匹配一个或多个空白符的正则表达式
	reg := regexp.MustCompile("\\s+")
	return reg.ReplaceAllString(str, "")
}
