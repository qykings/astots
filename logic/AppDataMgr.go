package logic

type AppFileStr struct {
	ClassName    string
	NameSpace    string
	Targeturl    string
	Targetdirurl string

	fileurl string
	File    *AsFileData
}

type AppDataMgr struct {
	classList []string
	classMap  map[string]AppFileStr
	ChangeTo  string
}

var instance *AppDataMgr

//var  once sync.Once

func AppDataMgrInstance() *AppDataMgr {
	if instance == nil {
		instance = &AppDataMgr{}
		instance.classMap = make(map[string]AppFileStr)
	}

	//once.Do(func() {
	//	instance=&AppdataMgr{}
	//})

	return instance
}

func (s *AppDataMgr) SetTargetDir(value string) {
	//s.Targetdirurl = value
}

func (s *AppDataMgr) AddClassName(className string,
	namespace string, targetdirurl, targeturl string,
	fileurl string, asfile *AsFileData) {

	for i := 0; i < len(s.classList); i++ {
		if s.classList[i] == className {
			println("已经有类名", className)
			return
		}

		//println(s.classList[i])
	}

	s.classList = append(s.classList, className)

	tmp := AppFileStr{}
	tmp.ClassName = className
	tmp.NameSpace = namespace
	tmp.Targetdirurl = targetdirurl
	tmp.Targeturl = targeturl
	tmp.fileurl = fileurl
	tmp.File = asfile

	s.classMap[className] = tmp

	//

}

func (s *AppDataMgr) GetAppFileStr(className string) (AppFileStr, bool) {
	tmp, result := s.classMap[className]
	if result {
		return tmp, true
	}

	if(className!=""){
		println("AppDataMgr 没有此类信息", className)
	}


	return tmp, result
}

func (s *AppDataMgr) DeleteAppFileStr(className string) {
	for i := 0; i < len(s.classList); i++ {
		if s.classList[i] == className {
			s.classList = append(s.classList[:i], s.classList[i+1:]...)
		}
	}
	delete(s.classMap, className)
}

func (s *AppDataMgr) GetClassList() []string {
	return s.classList
}
