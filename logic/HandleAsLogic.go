package logic

import (
	"as2ts/util"
	"pangu/cfile"
)

type HandleAsLogic struct {
}

func (s *HandleAsLogic) StartHandle() {
	list := AppDataMgrInstance().GetClassList()

	//检查有没有一个文件 写了多个类的
	for i := 0; i < len(list); i++ {
		tmp, isExist := AppDataMgrInstance().GetAppFileStr(list[i])
		if isExist {
			tmp.File.CheckOutClass()
		}

	}

	//
	//startTime := time.Now()
	for i := 0; i < len(list); i++ {
		tmp, isExist := AppDataMgrInstance().GetAppFileStr(list[i])

		//tmpstartTime := time.Now()
		if isExist {
			tmp.File.FindVarFun()
		}

		//constTime := time.Since(tmpstartTime)
		//fmt.Println(tmp.File.classname,"find var cost=[%s]", constTime)

	}

	//constTime := time.Since(startTime)
	//fmt.Println("all find cost=[%s]", constTime)

	for i := 0; i < len(list); i++ {
		tmp,isExist := AppDataMgrInstance().GetAppFileStr(list[i])
		if isExist{
			tmp.File.StartConvertTots()

			save(tmp.Targeturl, tmp.File.GetFileData())
		}

	}

}

func save(targetUrl string, filestr string) {
	targetUrl = util.ChangeUrl(targetUrl)

	//println("save:",targetUrl)
	wfile := new(cfile.File)
	wfile.WriteFile(targetUrl, filestr)

	format(targetUrl)
}

func format(fileurl string) {
	tmpstr := "tsfmt " + fileurl
	str, err := util.Exec_shell(tmpstr)
	if err == nil {
		//println(str)
		wfile := new(cfile.File)
		wfile.WriteFile(fileurl, str)
	}
}
