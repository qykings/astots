package logic

import "strings"

//type EgretClass string
//const (
//	ByteArray = "ByteArray"
//
////	Endian EgretClass="Endian"
//)
var EgretClass []string = []string{
	"ByteArray",
	"Sprite",
	"Endian",
	"Event",
	"EventDispatcher",
	"getTimer"}





func indeof(strArr []string, findstr string) bool {

	strLen := len(strArr)
	for i := 0; i < strLen; i++ {
		if strings.Compare(findstr, strArr[i]) == 0 {
			return true
		}
	}

	return false
}

func EgretString(classStr string) string {

	switch classStr {
	case "MouseEvent":
		classStr = "TouchEvent"
		 break
	}
 
	return "egret." + classStr + ";"
	 

}
