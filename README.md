#需要的类库
pangu
https://gitee.com/qykings/pangu.git

#### 介绍
go 语言的基础类库 

此项目如在win下运行会有路径问题，你们得自行去解决路径问题，
本项目在mac下开发，建议用mac、linux运行本项目


装以下库用于美化ts代码（在保存后程序会调shell去格式化代码）
https://github.com/vvakame/typescript-formatter/tree/7764258ad42ac65071399840d1b8701868510ca7

npm install -g typescript-formatter